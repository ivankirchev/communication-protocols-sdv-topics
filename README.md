# SDV Communication Protocols

## Overview
The purpose of [Eclipse uProtocol](https://projects.eclipse.org/projects/automotive.uprotocol) and this working group, shall be to bring the numerous automotive and Internet communication protocols together to build a set of sdks (developer libraries) and platforms (deployements) to enable "connecting applications and services running anywhere" (cloud, mobile, vehicle, infrastructure, etc...). 

uProtocol is the glue that makes the various technologies communicate with each other in a seamless way while also providing a consistent mental model for SDV software developers. 

We will be discussing the integration and support of the various technologies below to supporting uProtocol:  
- [Eclipse Zenoh](https://projects.eclipse.org/projects/iot.zenoh),
- [Eclipse Sommr](https://projects.eclipse.org/projects/automotive.sommr)
- [Eclipse eCAL](https://projects.eclipse.org/projects/automotive.ecal)
- [Eclipse p3comm](https://projects.eclipse.org/projects/automotive.p3com/governance)
- [Eclips iceoryx](https://projects.eclipse.org/projects/technology.iceoryx)
- [MQTT](https://mqtt.org/)
- [Eclipse Cyclone DDS] (https://projects.eclipse.org/projects/iot.cyclonedds)


We will house meeting minutes and roadmaps here but if you want running code, you have to go to GitHub :-). 

## Roadmap

https://github.com/orgs/eclipse-uprotocol/projects/3


## Meeting Minutes
Plesae visit each link below to get the charter of the meetings.


| Topic    | When? (CET) |
| -------- | ----------- |
| [Linux UPlatform](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/communication-protocols-sdv-topics/-/blob/main/MeetingNotes/Linux-uPlatform.md) | 12/4 3-5pm
| [uProtocol & Zenoh](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/communication-protocols-sdv-topics/-/blob/main/MeetingNotes/uProtocol-zenoh.md) | Wednesdays 2-3pm
| [uProtocl & Sommr](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/communication-protocols-sdv-topics/-/blob/main/MeetingNotes/uProtocol-sommr.md) | 12/6 3-4pm


