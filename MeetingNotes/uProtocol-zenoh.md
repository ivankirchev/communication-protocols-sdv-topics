# uProtocol & Zenoh Meeting

Weekly on Wednesdays 2-3:30pm CET

*Zoom Info:*
Join Zoom Meeting
https://eclipse.zoom.us/j/83187682865?pwd=CAxhUyIRY0PSXqcVw2XYuRxtvN1GVL.1

Meeting ID: 831 8768 2865
Passcode: 497074

## Purpose
The purpose of this weekly sync up meeting is the following:
- Define extensions to zenoh to support uProtocol attributes
- Status & updates for dev of the various uprotocol-[java|cpp|rust|python]-ulink-zenoh libraries
- Automotive ASIL-B requirements, safety, security, authorization & identity (when using zenoh within the vehicle)
- Shared memory support (ability to hook in vendor specific solutions to uTransport/zenoh)


**_NOTE:_** Discussions about the Zenoh & SommR "experiment" will be covered during the weekly uProtocol-SommR meeting

----------------
## Meeting Agenda 1/31/2024
- Shared Memory Sync up Update
  - New shared memory API (end of April) for zenoh 1.0
  - Discussed details of the implementation (memory allocation, error detection)
- [up-client-zenoh-rust PR#2]https://github.com/eclipse-uprotocol/up-client-zenoh-rust/pull/2#pullrequestreview-1838997364 missing source
  - Explanation of the uStreamer flows
- Zenoh Attachments & UAttributes update
  - **CY/Luca** for Status update
- [up-spec zenoh PR52](https://github.com/eclipse-uprotocol/up-spec/pull/52)
- CAP, **Steven** to setup meeting focused on this topic
  - Zenoh adding inceptors that can validate permissions on egress and ingress but identity is only passed and not verified by the transport
  - Luca to share information about sessions establishment & interceptor design. pub/private keys, password, random number generator. Need to 
  - Schedule meeting with Various folks to discuss
- **Steven:** Add task for adding uSubscription APIs to up-client-zenoh-rust and explain the flow
  & hooking subscribe() to z_subscriber() in lieu of inside registerListener()


## Meeting Agenda 1/24/2024
- uProtocol Rebranding: ulink --> upclient
- [up-client-zenoh-rust PR#2]https://github.com/eclipse-uprotocol/up-client-zenoh-rust/pull/2#pullrequestreview-1838997364 missing source
  - for request, the source address is  "who send the method" (ex. up:/device/hartley_app/1/rpc.response), this is not in UAttributes (it only has the sink)
  - For RPC response, the source is the method URI (ex. up/device/boss/1/rpc.Raise)
  - issue is with both RpcServer interface (RpcListener) and uTransport interface (UListener)
- Zenoh Attachments & UAttributes update **CY/Michael**
- [up-spec zenoh PR52](https://github.com/eclipse-uprotocol/up-spec/pull/52)


## Meeting Agenda 1/17/2024
- Zenoh Attachments & UAttributes Meeting summary **Michael**
- uprotocol-rust-zenoh-ulink updates
  - Code review & status **Peter**, **CY**, **Luca**
- UUri-2-Key expression mapping
- 

## Meeting Agenda 1/10/2024
- Catch up from last week
- Status updates for the various ulink library developments
  - Lots of progress on [uprotocol-rust-ulink-zenoh](https://github.com/ZettaScaleLabs/uprotocol-rust-ulink-zenoh/pull/2) shared by Luca CY
  - **Steven**, **Michael** & **Peter** to review and comment on functionality
- UUri-2-Key expression mapping
  - Zenoh uTransport *MUST* verify that UUri contains numbers (`is_micro()`)
  - Zenoh will use Micro (numbers only), no wildcard for source/sink addressing *HOWEVER* we need to double check this will not affect route optimizations at the transport layer

- Zenoh Attachments 
  - Initial prototype in [uprotocol-rust-ulink-zenoh](https://github.com/ZettaScaleLabs/uprotocol-rust-ulink-zenoh/pull/2) (copies the entire UAttributes). **Michael** to setup follow up meeting for optimizations


## Meeting Agenda 12/20/2023
- How to passing UAttributes using Attachment Feature (request, response, publish, etc...):
  - Zenoh Code Changes: https://github.com/eclipse-zenoh/zenoh-c/pull/203 & https://github.com/eclipse-zenoh/zenoh/pull/590
  - uAttributes-2-Zenoh Header/Attachments Mapping:
    - Need to optimally map uAttributes to Zenoh header attributes and use attachements for attributes that cannot be mapped (ex. UUID, token, etc...)
    - **Luca** working on initial prototype definitions in https://github.com/ZettaScaleLabs/uprotocol-rust-ulink-zenoh
- UUri to Key Expression Mapping: Need to be consistent
  - Initial discussion but did not conclude. Given the complexities of named URIs vs numbers we need to ensure proper mapping
  - Michael expressed issues with using UUri objects as we need quick way to map to z_publishers in the uTransport.send()
  - Discussion to be focused around this topic for next meeting
- uLink Library
  - Gave brief explanation but better summary will be posted on the main Eclipse-uProtocol README shortly


  - SDK Status
    - **Michael** uprotocol-cpp-ulink-zenoh
      - Plan to merge initial commit without the CAP and uSubscription hooks (that will be on a subsequent PR)
    - **CY** uprotocol-java-ulink-zenoh
      - No update
    - **Neelam** uprotocol-python-ulink-zenoh
      - No update
    - **Luca** uprotocol-rust-ulink-zenoh
      - WiP, initial uTransport & RpcClient skeleton is there

- Authorization & CAP: How to enforce per uE (zenoh peer) permissions per-topic 


## Meeting Agenda 12/13/2023

- **(Luca)** Status of zenoh header extensions
- **(Luca)** conan package support for zenoh-c per https://github.com/conan-io/conan-center-index/tree/master/docs/adding_packages 

### uProtocol-2-Zenoh Protocol Mapping **(Steven)**

#### Client-side (uLink Library) API Mapping
Just as ROS2 mapped Zenoh to their ROS APIs, so do we need to map uProtocol APIs and concepts to the zenoh transport APIs. Below are the proposed mapping for L1/L2 APIs.

_uProtocol-2-Zenoh API Mapping_
| uP                       | Zenoh C APIs        |
| ------------------------ | ------------------ |
| RpcClient.InvokeMethod() | z_get() |
| uTransport.send()        | z_declare_publisher(), z_publisher_put() |
| uTransport.registerListener() | z_declare_subscriber()   |
| uTransport.unregisterListener() | z_undeclare_subscriber() |
| RpcServer.registerRpcListener() | z_declare_queryable() |
| RpcServer.unregisterRpcListener() | z_undeclare_queryable()  |

#### uSubscription
Purpose of uSubscription is the following:
- Consistent set of Publisher APIs to fetch list of subscribers, creating topics, etc...
- Consistent set of Subscriber APIs to subscribe to topis published anywhere (local and remote). Coordinate subscriptions across different uPlatforms (using different underlining communication protocols)
- Permission management for subscriptions (a.k.a CAP) that involves verifying the identity of the subscriber and only permitting authorized subscribers to receive published events (vs anyone who subscribes can receive the published events)

Given above, the plan was to create a standalone zenoh peer (uE) that would implement the uSubscription APIs and business logic above. In order to realize the standalone uSubscription service, however, we need zenoh to support the following:
1. Ability to pass context (identity of the calling uE/ zenoh peer) over the queryable to uServices. This is so that services (like uSubscription, uDiscovery, all uServices) could implement CAP (authorize or deny access to subscriptions for uSubscription service, or method invocation for any other uServices) 
2. Only permit authorized subscribers from #1 above to be able to z_declare_subscriber() & receive published events, could this be using something like 
3. A uE (zenoh peer) is able to be notified of all subscriptions so that it can redirect remote subscription requests to external uPlatforms
4. The same zenoh peer is able to then subscribe on behalf of remote uEs 

##### Zenoh Session Establishment (Thank you ChatGPT)

In a Zenoh-peer network, the purpose of the Zenoh session establishment is to establish a communication session between two Zenoh peers.
When two Zenoh peers want to communicate with each other, they need to establish a session to exchange data and messages. The session establishment process involves the following steps:

1. Peer Discovery: The peers need to discover each other's presence in the network. This can be done through various mechanisms, such as multicast, unicast, or using a centralized discovery service.
2. Connection Initialization: Once the peers have discovered each other, they initiate a connection request to establish a direct communication link between them. This typically involves exchanging connection information, such as IP addresses and port numbers.
3. Authentication and Authorization: The peers may need to authenticate each other's identities and obtain authorization to communicate. This step ensures that only authorized peers can establish a session and exchange data.
4. Session Negotiation: During session establishment, the peers negotiate various session parameters, such as the communication protocol, quality of service (QoS) settings, and other configuration options. This negotiation ensures that both peers agree on the communication parameters for the session.
5. Session Establishment: Once the negotiation is complete and the parameters are agreed upon, the session is established between the two Zenoh peers. They are now connected and can start exchanging data and messages.

The Zenoh session establishment in a Zenoh-peer network enables direct communication between peers, allowing them to share data, publish and subscribe to topics, and perform other operations within the Zenoh ecosystem. It provides a foundation for building decentralized and distributed applications that can seamlessly communicate and share data across different peers in the network.

By establishing a session, Zenoh peers can leverage the efficient and flexible data distribution and storage capabilities provided by Zenoh, enabling them to build resilient and scalable systems that can dynamically adapt to changing network conditions and peer availability.

- Liveliness, Publisher Matching Status, Connectivity Status:
  - Connectivity Status and Events allow to keep track of local connectivity: https://github.com/eclipse-zenoh/roadmap/blob/main/rfcs/ALL/Connectivity%20Status%20and%20Events.md
  - Publisher Matching Status allows to know if a publisher currently has matching subscribers or not: https://github.com/eclipse-zenoh/roadmap/blob/main/rfcs/ALL/Matching%20Status.md
  - Liveliness allows to fine grain track the presence of given subscribers or any other entities: https://github.com/eclipse-zenoh/roadmap/blob/main/rfcs/ALL/Liveliness.md

#### Key Expression Mapping
UUri contains names and numbers, the UUri can be *serialized* into long form (represented as a `string` using names) like `/VIN/hartley.app/1/rpc.raise` short form (`string` using numbers) like `/VIN/33/1/2`, or micro form (byte array using numbers), or in UUri object form (containing both names and/or numbers). NOTE: Numbered UUris are used to reference topics published by mechatronic services (i.e. over SOME/IP). 

**Question to Zenoh community:** How Do we map uTransport UUri object (passed in uTransport APIs) to a zenoh key expressions?

### Minutes
- Identification & Authorization & CAP for Subscriptions in a Zenoh-Peer Network:
  - CAP is enforced by the uSubscription service (separate zenoh peer) and by the uServices (other zenoh peer)
  - Unique ID (UID) of the caller (to uSubscription or any other uService) is passed and then validated as part of the 4-way handshake during session establishment (before calling any other zenoh apis)
  - UID validation is used to ensure uProtocol SDKs (ulink library) is not bypassed (talking directly to zenoh)
  - Generation of the identifier (ex. UID) passed during session establishment, and the external verification of the identifier, is out of scope for the transport and eclipse as it will be platform/vendor specific. 
  - UID  
  - Client-side (uLink) library is responsible to implements uSubscription (client-side) and uTransport such that if the client is not authorized to receive the published events, it will not allow the peer to call z_declare_subscriber()
  - sessions 4-way handshake can ensure that peers cannot bypass ulink to use directly zenoh, and ulink can ensure that CAP is enforced at the client side (for subscriptions) HOWEVER how do we stop a valid uProtocol zenoh peers from using uLink to establish the session but then calling z_declare_subscriber() directly?
  - Luca Shared  https://github.com/eclipse-zenoh/zenoh/blob/master/io/zenoh-transport/src/unicast/establishment/ext/multilink.rs as an example of the extensions to sesison establishment that could be used for identification & authorization

- **Next Steps:** 
  - Continue to discuss CAP & topic above & Key expression mapping



## Meeting Agenda 12/6/2023

- **(Steven)** Introductions & Meeting best Practices
- **(Luca)** Status of zenoh header extensions
- **(Luca)** conan package support for zenoh-c per https://github.com/conan-io/conan-center-index/tree/master/docs/adding_packages 
  - To setup build recipe for clean library dependency management for C++
- uLink Library Status:
    - **(Michael)** C++
    - **(CK)** Java & Rust
    - **(Neelam)** Python
- **(Steven)** Hiccups with using Zenoh in uProtocol (See below)

### Zenoh & uProtocol Hiccup

With uProtocol 1.5 we've defined interfaces for uTransport, RpcClient with a clean & clear data model now defined in protos (UUri, UAttributes, UMessage) in uprotocol-core-api. We have also specified in 1.5 that the uLink library implements uTransport and RpcClient such that it can "connect" the uEs to the platform as seen in the figure below. Both the Android & Cloud uPlatforms fit well into this mental model as the dispatchers and the subscription management, are separate from the transport technology, (i.e. binder transport for Android and EventHub transport for Azure, etc...). It was simple to build centralized uSubscription service and controls the "fanning out" of published events based on permissions (i.e. CAP).   

![uProtocol](uprotocol.png)

With the ongoing development of zenoh based utransports (and uPlatforms), we've proved that it is very simple to implement the uTransport APIs using zenoh session, subscriber, and publisher interfaces HOWEVER the simple mental model (defined above), breaks down when we involve the core uEs. Unlike Android and Cloud transports and platforms, the zenoh "network" is a decentralized peer-2-peer network where subscription management and event dispatching is done by the uEs themselves and not through any centralized services (uSubscription service, dispatcher service, etc...). The de-centralized pub/sub architecture means that we cannot delegate permission management to some central authority (i.e. dispatchers or uSubscription service) and it means we cannot bridge subscriptions across transports/platforms (i.e. outsize of the zenoh world), the key reason for uProtocol!

![Zenoh Modes](zenoh-mode.png)

Given the above mentioned problems identified with zenoh integration (or any other distributed P2P transport protocols to that matter), there are a few solutions to be discussed:

**1. Client Mode:** All communication flows through a zenoh Router (i.e how we treated Binder, HTTP, MQTT, etc..)
- Client-side library (uLink) 
  - Zenoh ulink implements uTransport API (and wraps publisher & subscriber APIs)
  - client-side uSubscription simply does queriables to zenoh plugin
- uStreamer (Zenoh Router):
  - Implements uSubscription, uTwin service
  - CAP (authorization) for both queriables & subscription management

**2. Peer Mode:**
- Client-side library (uLink)
  - Implements RpcClient (wrapper for zenoh queriables) to supports invoking methods defined for uServices (uDiscovery)
  - Implements core uServices (uSubscription & uTwin)
  - Zenoh MUST implement CAP for queriables and for subscribing to topics (when we Session::declare_subscriber)
  - Service/producer performs CAP, requiring passing context (that is identifiable) 
  - There is no "uTransport"
- uStreamer (a.k.a Zenoh-Router):
  - Implement remote uSubscription logic
  - Bridge to other uTransports (SOME/IP, MQTT, etc....) 
**3. Other??**

- Notes from Meeting:
  - Capturing the requirements for the authorization and privacy issues regarding the uSubscription issue presented by Steven. Present requirements at the net meeting.
  - Present initial ideas (directions) to answer the above.
