# Sommr-uProtocol Integration Meetings

Bi-weekly meetings 3:30-4:30pm CET

*Zoom Info:*
Join Zoom Meeting
https://eclipse.zoom.us/j/88274093245?pwd=8wJWNL57bnzs5foAFUB3qulNfhGaf4.1

Meeting ID: 882 7409 3245
Passcode: 224371

## Purpose
Work together as a community to build a sommr SOME/IP-based uProtocol transport. The meetings will be to cover:
- Defining the uprotocol-spec uTransport specifications [sommr.adoc](https://github.com/eclipse-uprotocol/uprotocol-spec/blob/main/up-l1/sommr.adoc) to map UAttributes, uPayload, UUri to SOME/IP and sommr implementation.  (old WiP to be shared)
- Discuss languages, timeline, SoW
- Features/functionality required (i.e. SecOC, SOME/IP-SD, serialization formats, etc...)
- timelines, deliverables, etc... 

## Meeting Agenda 1/24/2024
- **Bern**/**Ansgar**: Availability of SommR Code
- **Peter**: Linux uStreamer & Test App Progress 
- **Steven**: [Backlog & Milestone Review](https://github.com/orgs/eclipse-uprotocol/projects/4/views/1)

## Meeting Agenda 1/17/2024
- **Peter**: Linux uStreamer & Test App Progress 
- **Bern**/**Ansgar**: Availability of SommR Code
- **Steven**: [Backlog & Milestone Review](https://github.com/orgs/eclipse-uprotocol/projects/4/views/1)


## Meeting Agenda 12/20/2023
- **Tom F.:**  Open source of SommR update (source code, documentation, AUTOSAR IP)
  - Un able to join, will follow up in 2 weeks
- January Prototype Kick off:
  - uprotocol-rust-transport-sommr: uTransport Implementation using SommR (to be used by uStreamer)
  - uprotocol-platform-linux-zenoh: Skeleton streamer
  - Use Cases:
    - Zenoh peer receiving uService published event
    - Zenoh peer invoking method in uService and receiving a reply

![uProtocol-SommR PoC](sommR.drawio.png)

  - The PoC will be elaborated further Steven to figure out where to document this effort and to provide clear outcomes for the project
  - Kick off will be January 8th 

## Meeting Agenda 12/6/2023

- Introductions & Meeting best Practices
- Kick off discussions
- sommr Open Source Status
  - December closing AUTOSAR IP reviews
  - January/February Target for approval and release
  - sommr is daemon is written in rust with C++ & Rust bindings
  - Runs on SoCs (in the high-compute space) and is fully tested and validated against AUTOSAR classic SOME/IP stacks  
- Action Items: 
  - **Tom:** 
    - Releasing documentation for sommr
    - List of features/functionality (what is and isn't supported) if not in documentation
    - check in what is needed for ASIL-B certification of Rust compiler
  - **Daniel:**  
    - Could Eclipse SDV working group allocate any support (i.e. like ThreadX) for certifications of joint eclipse SDV projects?
  - **Steven:** 
    - Architecture diagram for SOME/IP bridge (for Android & Linux uPlatforms)
    - SOME/IP serialization/deserializartion extensions for protobuf
    - Next Meeting 12/20


